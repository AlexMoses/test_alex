// Граф рассадки:

// [1] - [2]   [11] - [12] 
// |     |       |     |
// [3] - [4]   [13] - [14] 
// |     |       |     |
// [5] - [6]   [15] - [16] 
// |     |
// [7] - [8]
// |     |
// [9] - [10]

// Массив граней графа (вершины, между которыми есть грань). Грани ставим между теми вершинами, за которыми сотрудиники не могут работать в один день
const edges = [
    [1,2],
    [1,3],
    [2,4],
    [3,4],
    [5,6],
    [3,5],
    [4,6],
    [7,8],
    [5,7],
    [6,8],
    [7,9],
    [8,10],
    [9,10],
    [11,12],
    [11,13],
    [12,14],
    [13,14],
    [13,15],
    [14,16],
    [15,16],
  ];

// Раписание работы сотрудников. [ номер вершины (место) сотрудника, количество дней в неделю]
timetable = [
    [1, 1],
    [2, 3],
    [3, 4],
    [4, 2],
    [5, 3],
    [6, 2],
    [7, 1],
    [8, 3],
    [9, 1],
    [10, 1],
    [11, 2],
    [12, 4],
    [13, 1],
    [14, 3],
    [15, 2],
    [16, 2],
];

let nodes = []; // массив всех вершин
let inOffice = []; //массив тех вершин, которые работают в офисе

class Node {
  constructor(id) {
    this.id = id;
    this.nodes = []
  }

  addNode(node){
    this.nodes.push(node);
  }

  addColor(color){
    this.color = color;
  }

  setDays(days){
      this.days = days;
  }
}

let init = () => {
  // по граням строим граф и создаем вершины
  edges.forEach((edge) => {
    edge.forEach((node) => {
      if (!nodes.find(item => item.id == node)) {
        nodes.push(new Node(node));
      }
    });

    const first = nodes.find(item => item.id == edge[0]);
    const second = nodes.find(item => item.id == edge[1]);
    first.addNode(second);
    second.addNode(first);
  });

  // По расписанию ставим дни для вершины
  timetable.forEach((item) => {
    const node = nodes.find(node => node.id == item[0]);
    node.setDays(item[1]);
  })
};

// сортируем массив вершин по убыванию приоритета. Приорите - количество дней, которые нужно отработать сотруднику в офисе
sort = () => {
    compare = (a, b) => {
        return b.days - a.days;
    }
    nodes.sort(compare);
}

// Расскрашиваем граф. Если сотрудник выходит в офис, то color = true, иначе false. Смотри на соседей вершины, если никто не выходит в офис, значит можем выпустить сотрудника в офис.
coloring = () => {
    nodes.forEach((node) => {
        if (node.nodes.every(node => node.color !== true) && node.days > 0 && inOffice.length < nodes.length) {
            node.color = true;
            inOffice.push(node.id);
        } else {
            node.color = false;
        }
    });
    console.log('in office', inOffice);
}

// После каждого дня обновляем приоритет. Те, кто выходили в офис, количество дней уменьшаем.
updateDays = () => {
    nodes.forEach((node) => {
        if (node.color == true) {
            node.days--;
        }
        node.color = null;
    });

    inOffice = [];
}

init();
sort();
coloring();
updateDays();

sort();
coloring();
updateDays();

sort();
coloring();
updateDays();

sort();
coloring();
updateDays();

sort();
coloring();
updateDays();